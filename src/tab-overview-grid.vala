[GtkTemplate (ui = "/org/example/TabOverview/tab-overview-grid.ui")]
public class TabsMobile.TabOverviewGrid : Gtk.Widget {
    public signal void select (Adw.TabPage page);
    public signal void create ();

    [GtkChild]
    private unowned PopoverBin popover_bin;
    [GtkChild]
    private unowned Gtk.ScrolledWindow swindow;
    [GtkChild]
    private unowned Gtk.FlowBox flowbox;

    private Adw.TabView _view;
    public Adw.TabView view {
        get { return _view; }
        set {
            if (view == value)
                return;

            if (view != null) {
                // disconnect
            }

            _view = value;

            if (view != null) {
                pages = view.pages;
                var model = new PlaceholderModel (pages);

                flowbox.bind_model (model, obj => create_item (obj as PlaceholderModelItem));

                view.notify["n-pages"].connect (update_n_pages);
                update_n_pages ();

                view.notify["selected-page"].connect (() => {
                    update_selection ();
                });
            }
        }
    }

    public float thumbnail_xalign { get; set; default = 0; }
    public float thumbnail_yalign { get; set; default = 0; }

    private Gtk.SelectionModel pages;

    private List<Gdk.Paintable> paintables;

    private Gtk.Widget placeholder_item;
    private Gtk.Popover popover;

    static construct {
        set_layout_manager_type (typeof (Gtk.BinLayout));

        typeof (PopoverBin).ensure ();
    }

    construct {
        paintables = new List<Gdk.Paintable> ();
    }

    [GtkCallback]
    private void child_activated_cb (Gtk.Widget child) {
        var page = child.get_data<Adw.TabPage>("page");

        if (page == null) {
            if (placeholder_item != null)
                placeholder_item.hide ();

            create ();

            return;
        }

        // TODO block the notify::selected-page handler
        select (page);
    }

    [GtkCallback]
    private void press_cb (Gtk.GestureClick gesture, int n_presses, double x, double y) {
        if (n_presses > 1) {
            gesture.set_state (Gtk.EventSequenceState.DENIED);
            return;
        }

        var event = gesture.get_current_event ();

        if (!event.triggers_context_menu ())
            return;

        var child = flowbox.get_child_at_pos ((int) x, (int) y);

        if (child == null) {
            gesture.set_state (Gtk.EventSequenceState.DENIED);
            return;
        }

        var page = child.get_data<Adw.TabPage> ("page");

        if (page == null) {
            gesture.set_state (Gtk.EventSequenceState.DENIED);
            return;
        }

        var menu_model = view.menu_model;

        if (menu_model == null) {
            gesture.set_state (Gtk.EventSequenceState.DENIED);
            return;
        }

        view.setup_menu (page);

        if (popover == null) {
            popover = new Gtk.PopoverMenu.from_model_full (menu_model, Gtk.PopoverMenuFlags.NESTED);
            popover_bin.popover = popover;
            popover.has_arrow = false;
            popover.position = Gtk.PositionType.BOTTOM;
            popover.halign = Gtk.Align.START;

            popover.notify["visible"].connect (() => {
                if (popover.visible)
                    return;

                Idle.add (() => {
                    view.setup_menu (null);

                    return Source.REMOVE;
                });
            });
        }

        double px, py;
        flowbox.translate_coordinates (popover_bin, x, y, out px, out py);

        popover.set_pointing_to ({ (int) px, (int) py, 0, 0 });
        popover.popup ();

        gesture.set_state (Gtk.EventSequenceState.CLAIMED);
        gesture.reset ();
    }

    [GtkCallback]
    private void release_cb (Gtk.GestureClick gesture, int n_presses, double x, double y) {
        uint button = gesture.get_current_button ();
        if (n_presses > 1 || button != Gdk.BUTTON_MIDDLE) {
            gesture.set_state (Gtk.EventSequenceState.DENIED);
            return;
        }

        var child = flowbox.get_child_at_pos ((int) x, (int) y);

        if (child == null) {
            gesture.set_state (Gtk.EventSequenceState.DENIED);
            return;
        }

        var page = child.get_data<Adw.TabPage> ("page");

        if (page == null) {
            gesture.set_state (Gtk.EventSequenceState.DENIED);
            return;
        }

        view.close_page (page);
        gesture.set_state (Gtk.EventSequenceState.CLAIMED);
        gesture.reset ();
    }

    protected override void dispose () {
        if (popover_bin != null)
            popover_bin.unparent ();

        base.dispose ();
    }

    private void update_n_pages () {
        int n_pages = view.n_pages;

        if (placeholder_item != null && placeholder_item.visible)
            n_pages++;

        flowbox.max_children_per_line = n_pages.clamp (2, 100);
    }

    public void will_open () {
        update_selection ();
    }

    public void did_close () {
        placeholder_item.show ();
        update_n_pages ();
    }

    public Gtk.Widget get_current_thumbnail () {
        uint pos = view.get_page_position (view.selected_page);
        var child = flowbox.get_child_at_index ((int) pos);

        return child.get_data<Gtk.Widget> ("thumbnail");
    }

    public Gdk.Paintable get_current_paintable () {
        uint pos = view.get_page_position (view.selected_page);
        var child = flowbox.get_child_at_index ((int) pos);

        return child.get_data<TabPaintable> ("paintable");
    }

    public void update_paintables () {
        foreach (var paintable in paintables)
            paintable.invalidate_size ();
    }

    private void update_selection () {
        if (!get_mapped ())
            return;

        if (view.selected_page == null)
            return;

        uint pos = view.get_page_position (view.selected_page);
        var child = flowbox.get_child_at_index ((int) pos);

        Graphene.Rect bounds = {};
        child.compute_bounds (flowbox, out bounds);

        double lower = bounds.origin.y + flowbox.margin_top;
        double upper = lower + bounds.size.height;

        swindow.vadjustment.clamp_page (lower, upper);
    }

    private Gtk.Widget create_item (PlaceholderModelItem item) {
        var child = new Gtk.FlowBoxChild ();

        var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 12);
        child.child = box;

        var bin = new Gizmo ();
        bin.layout_manager = new Gtk.BinLayout ();
        bin.width_request = 119;
        bin.hexpand = true;
        bin.vexpand = true;
        box.append (bin);

        child.set_data<Gtk.Widget> ("thumbnail", bin);

        var hbox = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 6);
        box.append (hbox);

        var icon = new Gtk.Image ();
        icon.pixel_size = 16;
        hbox.append (icon);

        var label = new Gtk.Label (null);
        label.single_line_mode = true;
        label.ellipsize = Pango.EllipsizeMode.END;
        label.max_width_chars = 0;
        label.hexpand = true;
        label.xalign = 0;
        hbox.append (label);

        if (item.is_placeholder) {
            var image = new Gtk.Image ();
            image.icon_name = "list-add-symbolic";
            image.icon_size = Gtk.IconSize.LARGE;
            image.halign = CENTER;
            image.valign = CENTER;
            image.set_parent (bin);

            bin.add_css_class ("tab-placeholder");

            placeholder_item = child;
        } else {
            var page = item.item as Adw.TabPage;

            var paintable = new TabPaintable (page.child, view, 0.8, 1.6);
            bind_property ("thumbnail-xalign", paintable, "xalign", BindingFlags.SYNC_CREATE);
            bind_property ("thumbnail-yalign", paintable, "yalign", BindingFlags.SYNC_CREATE);
            paintables.prepend (paintable);
            child.set_data<TabPaintable> ("paintable", paintable);

            var picture = new Gtk.Picture ();
            picture.add_css_class ("tab-thumbnail");
            picture.overflow = Gtk.Overflow.HIDDEN;
            picture.paintable = paintable;
            picture.destroy.connect (() => {
                paintables.remove (paintable);
            });
            picture.set_parent (bin);

            child.set_data<Adw.TabPage> ("page", page);

            var close = new Gtk.Button ();
            close.halign = Gtk.Align.END;
            close.valign = Gtk.Align.START;
            close.add_css_class ("flat");
            close.add_css_class ("tab-close");
            close.icon_name = "window-close-symbolic";

            close.clicked.connect (() => {
                view.close_page (page);
            });
            close.set_parent (bin);

            page.bind_property ("icon", icon, "gicon", BindingFlags.SYNC_CREATE);
            page.bind_property ("title", label, "label", BindingFlags.SYNC_CREATE);
            page.bind_property ("pinned", close, "visible", BindingFlags.SYNC_CREATE | BindingFlags.INVERT_BOOLEAN);
        }

        return child;
    }
}
