private class TabsMobile.TabPaintable : Object, Gdk.Paintable {
    public Gtk.Widget widget { get; construct; }
    public Adw.TabView view { get; construct; }
    public double min_ratio { get; construct; }
    public double max_ratio { get; construct; }

    private Gdk.Paintable paintable;
    private Gdk.Paintable? cached_paintable;

    private Gdk.RGBA cached_bg;
    private int cached_width;
    private int cached_height;
    private bool schedule_clear_cache;

    public float xalign { get; set; default = 0; }
    public float yalign { get; set; default = 0; }

    public TabPaintable (Gtk.Widget widget, Adw.TabView view, double min_ratio, double max_ratio) {
        Object (widget: widget, view: view, min_ratio: min_ratio, max_ratio: max_ratio);
    }

    construct {
        paintable = new Gtk.WidgetPaintable (widget);

        paintable.invalidate_contents.connect (() => {
            invalidate_contents ();
        });
        paintable.invalidate_size.connect (() => {
            invalidate_size ();
        });

        invalidate_contents.connect (() => {
            if (schedule_clear_cache) {
                cached_paintable = null;
                schedule_clear_cache = false;
            }
        });

        widget.unmap.connect (() => {
            cached_paintable = get_current_image ();
            cached_width = view.get_width ();
            cached_height = view.get_height ();
            if (!view.get_style_context ().lookup_color ("theme_bg_color", out cached_bg))
                cached_bg = { 1, 0, 0, 1 };
        });

        widget.map.connect (() => {
            invalidate_contents ();

            if (cached_paintable != null)
                schedule_clear_cache = true;
       });
/*
        widget.get_settings ().notify["gtk-application-prefer-dark-theme"].connect (() => {
            if (cached_paintable != null)
                schedule_clear_cache = true;

            invalidate_contents ();
        });
*/
        notify["xalign"].connect (invalidate_contents);
        notify["yalign"].connect (invalidate_contents);
    }

    private void snapshot_paintable (Gtk.Snapshot snapshot, double width, double height, Gdk.Paintable paintable) {
        double snapshot_ratio = width / height;
        double paintable_ratio = paintable.get_intrinsic_aspect_ratio ();

        if (paintable_ratio > snapshot_ratio) {
            double new_width = width * paintable_ratio / snapshot_ratio;
            snapshot.translate ({ (float) (width - new_width) * xalign, 0 });
            width = new_width;
        } if (paintable_ratio < snapshot_ratio) {
            double new_height = height * snapshot_ratio / paintable_ratio;
            snapshot.translate ({ 0, (float) (height - new_height) * yalign });
            height = new_height;
        }

        paintable.snapshot (snapshot, width, height);
    }

    private void snapshot (Gdk.Snapshot snapshot, double width, double height) {
        var gtk_snapshot = snapshot as Gtk.Snapshot;
        Graphene.Rect bounds = { { 0, 0 }, { (float) width, (float) height } };

        if (cached_paintable != null) {
            gtk_snapshot.append_color (cached_bg, bounds);

            snapshot_paintable (gtk_snapshot, width, height, cached_paintable);
            return;
        }

        if (!widget.get_mapped ())
            return;

        Gdk.RGBA bg;
        if (!view.get_style_context ().lookup_color ("theme_bg_color", out bg))
            bg = { 1, 0, 0, 1 };
        gtk_snapshot.append_color (bg, bounds);

        snapshot_paintable (gtk_snapshot, width, height, paintable);
    }

    private Gdk.Paintable get_current_image () {
        return cached_paintable ?? paintable.get_current_image ();
    }

    private double get_intrinsic_aspect_ratio () {
        double ratio = (double) view.get_width () / view.get_height ();

        return ratio.clamp(min_ratio, max_ratio);
    }

    private int get_intrinsic_width () {
        return 360;
    }
}
