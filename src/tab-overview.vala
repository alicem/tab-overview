public class TabsMobile.TabOverview : Gtk.Widget, Gtk.Buildable {
    public signal Adw.TabPage create_page ();

    private const int OPEN_DURATION = 250;

    private const string SHADER = """
uniform vec2 origin;
uniform vec2 size;
uniform float opacity;

uniform sampler2D u_texture1;

void mainImage(out vec4 fragColor,
               in vec2 fragCoord,
               in vec2 resolution,
               in vec2 uv) {
    if (fragCoord.x >= origin.x && fragCoord.x <= origin.x + size.x &&
        fragCoord.y >= origin.y && fragCoord.y <= origin.y + size.y)
        fragColor = vec4(0, 0, 0, 0);
    else
        fragColor = opacity * GskTexture(u_texture1, uv);
}
""";

    private Adw.TabView _view;
    public Adw.TabView view {
        get { return _view; }
        set {
            if (view == value)
                return;

            _view = value;
        }
    }

    private Gtk.Widget _child;
    public Gtk.Widget child {
        get { return _child; }
        set {
            if (child == value)
                return;

            if (child != null)
                child.unparent ();

            _child = value;

            if (child != null)
                child.insert_after (this, null);
        }
    }

    private bool _is_open;
    public bool is_open {
        get { return _is_open; }
        set {
            if (is_open == value)
                return;

            _is_open = value;

            if (is_open || animation != null) {
                if (animation != null)
                    animation.stop ();

                grid.show ();
                grid.can_target = true;
            }

            if (is_open)
                grid.will_open ();

            // FIXME should be weak ref in C
            hidden_thumbnail = grid.get_current_thumbnail ();
            hidden_thumbnail.opacity = 0;

            transition = grid.get_current_paintable ();

            if (is_open)
//                animation = new Animation (this, progress, 1, 2500);
                animation = new SpringAnimation.with_damping_ratio (this, progress, 1, 0, 0.85, 1, 300, 0.0005);
            else
//                animation = new Animation (this, progress, 0, 2500);
                animation = new SpringAnimation.with_damping_ratio (this, progress, 0, 0, 0.85, 1, 300, 0.0005);
            set_progress (progress);
            animation.notify["value"].connect (() => {
                set_progress (animation.value);
            });
            animation.done.connect (() => {
                animation = null;

                if (hidden_thumbnail != null) {
                    hidden_thumbnail.opacity = 1;
                    hidden_thumbnail = null;
                }

                transition = null;

                if (!is_open) {
                    grid.did_close ();
                    grid.hide ();
                }
            });
            animation.start ();

            if (is_open)
                add_css_class ("open");
            else
                remove_css_class ("open");
        }
    }

    public float thumbnail_xalign { get; set; default = 0; }
    public float thumbnail_yalign { get; set; default = 0; }

    private SpringAnimation? animation;
    private double progress;

    private Gsk.GLShader? shader;
    private bool shader_compiled;

    private TabOverviewGrid grid;

    private Gdk.Paintable? transition;
    private Gtk.Widget hidden_thumbnail;

    private void set_progress (double progress) {
        this.progress = progress;

        queue_draw ();
    }

    static construct {
        set_css_name ("switcher");

        add_binding (Gdk.Key.Escape, 0, widget => {
            var self = widget as TabOverview;

            if (!self.is_open)
                return Gdk.EVENT_PROPAGATE;

            self.is_open = false;

            return Gdk.EVENT_STOP;
        }, null);
    }

    construct {
        overflow = Gtk.Overflow.HIDDEN;

        grid = new TabOverviewGrid ();
        grid.visible = false;
        grid.add_css_class ("scrim");
        grid.select.connect (page => {
            view.selected_page = page;
            is_open = false;
        });
        grid.create.connect (() => {
            var page = create_page ();
            view.selected_page = page;
            is_open = false;
        });
        bind_property ("view", grid, "view", BindingFlags.SYNC_CREATE);
        bind_property ("thumbnail-xalign", grid, "thumbnail-xalign", BindingFlags.SYNC_CREATE);
        bind_property ("thumbnail-yalign", grid, "thumbnail-yalign", BindingFlags.SYNC_CREATE);
        grid.set_parent (this);

        var gesture = new Gtk.GestureZoom ();
        gesture.propagation_phase = CAPTURE;

        gesture.scale_changed.connect (scale => {
            if (scale < 1 && !is_open) {
                gesture.set_state (CLAIMED);
                is_open = true;
            } else {
                gesture.set_state (DENIED);
            }

            gesture.reset ();
        });

        add_controller (gesture);
    }

    protected override void dispose () {
        child = null;

        if (grid != null) {
            grid.unparent ();
            grid = null;
        }

        base.dispose ();
    }

    private void add_child (Gtk.Builder builder, Object object, string? type) {
        if (object is Gtk.Widget)
            child = object as Gtk.Widget;
        else
            base.add_child (builder, object, type);
    }

    protected override void measure (Gtk.Orientation orientation, int for_size, out int minimum, out int natural, out int minimum_baseline, out int natural_baseline) {
        minimum = 0;
        natural = 0;
        minimum_baseline = -1;
        natural_baseline = -1;

        for (var c = get_first_child (); c != null; c = c.get_next_sibling ()) {
            if (!c.should_layout ())
                continue;

            int child_min = 0, child_nat = 0;
            int child_min_baseline = -1;
            int child_nat_baseline = -1;

            c.measure (orientation, for_size, out child_min, out child_nat,
                       out child_min_baseline, out child_nat_baseline);

            minimum = int.max (minimum, child_min);
            natural = int.max (natural, child_nat);

            if (child_min_baseline > -1)
                minimum_baseline = int.max (minimum_baseline, child_min_baseline);

            if (child_nat_baseline > -1)
                natural_baseline = int.max (natural_baseline, child_nat_baseline);
        }
    }

    protected override void size_allocate (int width, int height, int baseline) {
        if (child != null)
            child.allocate (width, height, baseline, null);

        grid.update_paintables ();
        grid.allocate (width, height, baseline, null);
    }

    private void ensure_shader () {
        if (shader != null)
            return;

        var bytes = new Bytes.static ((uint8[]) SHADER);
        shader = new Gsk.GLShader.from_bytes (bytes);

        var renderer = get_native ().get_renderer ();

        try {
            shader_compiled = shader.compile (renderer);
        }
        catch (Error e) {
            critical (e.message);
            /* We don't really care about errors here */
            shader_compiled = false;
        }
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        if (animation == null || progress == 0) {
            if (progress > 0.5 && grid != null)
                snapshot_child (grid, snapshot);

            /* We don't want to actually draw the child, but we do need it
             * to redraw so that it can be displayed by the paintables */
            if (child != null) {
                var child_snapshot = new Gtk.Snapshot ();
                snapshot_child (child, child_snapshot);
                var node = child_snapshot.free_to_node ();
                if (progress <= 0.5)
                    snapshot.append_node (node);
            }

            return;
        }

        Graphene.Rect bounds = {};
        Graphene.Rect view_bounds = {};
        Graphene.Rect card_bounds = {};
        Graphene.Vec2 origin = {};
        Graphene.Vec2 size = {};
        Graphene.Vec2 offset_strength = {};

        ensure_shader ();

        bounds.init (0, 0, get_width (), get_height ());

        assert (view.compute_bounds (this, out view_bounds));
        if (!hidden_thumbnail.compute_bounds (this, out card_bounds))
            card_bounds = Graphene.Rect.zero ();

        origin.init (view_bounds.origin.x, view_bounds.origin.y);
        size.init (view_bounds.size.width, view_bounds.size.height);
        offset_strength.init (0.0f, 0.0f);

        var view_center = view_bounds.get_center ();
        float scale_x = 1 - 0.1f * (float) (1 - progress.clamp (0, 1));
        float scale_y = 1 - 0.1f * (float) (1 - progress.clamp (0, 1));

        Gdk.RGBA border;
        if (!view.get_style_context ().lookup_color ("borders", out border))
            border = { 1, 0, 0, 1 };

        var thumbnail_bounds = view_bounds.interpolate (card_bounds, progress);
        float w = thumbnail_bounds.size.width;
        float h = thumbnail_bounds.size.height;

        Gsk.RoundedRect thumbnail_rect = {};
        thumbnail_rect.init_from_rect ({{ 0, 0}, {w, h}}, (float) (8 * progress));

        snapshot.save ();
        snapshot.push_opacity (progress);
        snapshot.translate (view_center);
        snapshot.scale (scale_x, scale_y);
        snapshot.translate ({ -view_center.x, -view_center.y });
        snapshot_child (grid, snapshot);
        snapshot.pop ();
        snapshot.restore ();

        snapshot.save ();
        snapshot.translate (thumbnail_bounds.origin);
        snapshot.append_outset_shadow (thumbnail_rect, border, 0, 0, 1, 0);
        snapshot.push_rounded_clip (thumbnail_rect);
        transition.snapshot (snapshot, w, h);
        snapshot.pop ();
        snapshot.restore ();

        scale_x += 0.1f;
        scale_y += 0.1f;
        snapshot.save ();
        snapshot.translate (view_center);
        snapshot.scale (scale_x, scale_y);
        snapshot.translate ({ -view_center.x, -view_center.y });

        var snapshot2 = new Gtk.Snapshot ();

        if (shader_compiled) {
            snapshot2.push_gl_shader (
                shader,
                bounds,
                shader.format_args (
                    opacity: 1 - progress.clamp (0, 1),
                    origin: origin,
                    size: size
                )
            );
        }

        snapshot_child (child, snapshot2);

        if (shader_compiled) {
            snapshot2.gl_shader_pop_texture ();
            snapshot2.pop ();
        }

        var node = snapshot2.free_to_node ();
        snapshot.append_node (node);

        snapshot.restore ();
    }
}
