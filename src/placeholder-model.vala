public class TabsMobile.PlaceholderModelItem : Object {
    public Object? item { get; construct; }
    public bool is_placeholder {
        get { return item == null; }
    }

    internal PlaceholderModelItem (Object? item) {
        Object (item: item);
    }
}

public class TabsMobile.PlaceholderModel : Object, ListModel {
    public ListModel model { get; construct; }

    public PlaceholderModel (ListModel model) {
        Object (model: model);
    }

    construct {
        model.items_changed.connect ((pos, removed, added) => {
            items_changed (pos, removed, added);
        });
    }

    private Type get_item_type () {
        return model.get_item_type ();
    }

    private uint get_n_items () {
        return model.get_n_items () + 1;
    }

    private Object? get_item (uint pos) {
        if (pos < model.get_n_items ())
            return new PlaceholderModelItem (model.get_item (pos));

        return new PlaceholderModelItem (null);
    }
}
